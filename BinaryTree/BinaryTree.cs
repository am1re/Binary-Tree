﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BinaryTree
{
    public class BinaryTree<T> : IEnumerable<T>
    {
        private BinaryTreeNode<T> root;

        /// <summary>
        /// Event that should be called when new element is added
        /// </summary>
        public event EventHandler ElementAdd;

        /// <summary>
        /// Event that should be called when element in tree is removed
        /// </summary>
        public event EventHandler ElementRemove;

        /// <summary>
        /// Defines how many elements tree contains
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// There should be created private readonly delegate field (custom or from library)
        /// that will be tree comparer and used in Add() method
        /// Like method in interface <see cref="IComparer<T>"/> it has to have same signature
        /// Parameters: T, T
        /// </summary>
        /// <param name="x">The first object to compare</param>
        /// <param name="y">The second object to compare</param>
        /// <returns>
        /// A signed integer that indicates the relative values of x and y, as shown in the
        //     following table. Value Meaning Less than zero x is less than y. Zero x equals
        //     y. Greater than zero x is greater than y.
        /// </returns>
        private readonly Func<T, T, int> compare;

        /// <summary>
        /// Checks if type T implements <see cref="IComparable<T>"/>
        /// If it does: saves that as default comparer
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when T doesn't implement <see cref="IComparable<T>"</exception>
        public BinaryTree()
        {

            if (!typeof(T).GetInterfaces()
                .Any(i => i.IsGenericType
                && i.GetGenericTypeDefinition() == typeof(IComparable<>)))
            {
                throw new ArgumentException($"{typeof(T)} is not implementing {typeof(IComparable<T>)}");
            }

            compare = (T x, T y) =>
            {
                return (x as IComparable<T>).CompareTo(y);
            };
        }

        /// <summary>
        /// Creates instance of tree and saves comparer in local variable        /// </summary>
        /// <param name="comparer"><see cref="IComparer<T>"/></param>
        public BinaryTree(IComparer<T> comparer)
        {
            //TODO: Write method of comparing to a delegate field created before

            compare = comparer.Compare;
        }

        /// <summary>
        /// Adds element to the tree according to comparer
        /// </summary>
        /// <param name="item">Object that should be added in tree</param>
        public void Add(T item)
        {

            var result = AddNode(new BinaryTreeNode<T>(item));
            ++Count;

        }

        /// <summary>
        /// Removes element from tree by its reference
        /// </summary>
        /// <param name="item">Object that should be removed from tree</param>
        /// <returns>True if element was deleted succesfully, false if element wasn't found in tree</returns>
        public bool Remove(T item)
        {
            if (ReferenceEquals(item, null))
            {
                return false;
            }

            var node = FindNode(root, item);
            if (node == null) return false;

            var result = RemoveNode(node);
            if (result == false) return false;

            --Count;

            return true;
        }

        /// <summary>
        /// Returns item with the highest value
        /// </summary>
        /// <returns>The element with highest value</returns>
        /// <exception cref="InvalidOperationException">Thrown if tree is empty</exception> 
        public T TreeMax()
        {
            if (root == null)
            {
                throw new InvalidOperationException("Sequence contains no elements");
            }

            return MaxNode(root).Data;
        }

        /// <summary>
        /// Returns item with the lowest value
        /// </summary>
        /// <returns>The element with lowest value</returns>
        /// <exception cref="InvalidOperationException">Thrown if tree is empty</exception>
        public T TreeMin()
        {
            if (root == null)
            {
                throw new InvalidOperationException("Sequence contains no elements");
            }

            return MinNode(root).Data;
        }

        /// <summary>
        /// Checks if tree contains element by its reference
        /// </summary>
        /// <param name="item">Object that should (or not) be found in tree</param>
        /// <returns>True if tree contains item, false if it doesn't</returns>
        public bool Contains(T data)
        {
            if (data == null)
            {
                return false;
            }

            return FindNode(root, data) != null;
        }

        /// <summary>
        /// Makes tree traversal
        /// </summary>
        /// <param name="traverseType"><see cref="TraverseType"></param>
        /// <returns>Sequense of elements of tree according to traverse type</returns>
        public IEnumerable<T> Traverse(TraverseType traverseType)
        {
            Func<BinaryTreeNode<T>, IEnumerable<BinaryTreeNode<T>>> traverseFunc = null;

            switch (traverseType)
            {
                case TraverseType.InOrder:
                    traverseFunc = InOrderTraversal;
                    break;
                case TraverseType.PostOrder:
                    traverseFunc = PostOrderTraversal;
                    break;
                case TraverseType.PreOrder:
                    traverseFunc = PreOrderTraversal;
                    break;
            }

            if (root != null && traverseFunc != null)
            {
                foreach (var node in traverseFunc(root))
                {
                    yield return node.Data;
                }
            }
        }

        /// <summary>
        /// Makes in-order traverse
        /// Serves as a default <see cref="TraverseType"/> for tree
        /// </summary>
        /// <returns>Enumerator for iterations in foreach cycle</returns>
        public IEnumerator<T> GetEnumerator()
        {
            foreach (var node in Traverse(TraverseType.InOrder))
            {
                yield return node;
            }
        }

        /// <summary>
        /// Makes in-order traverse
        /// Serves as a default <see cref="TraverseType"/> for tree
        /// </summary>
        /// <returns>Enumerator for iterations in foreach cycle</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private BinaryTreeNode<T> MaxNode(BinaryTreeNode<T> node)
        {
            while (node.RightNode != null)
            {
                node = node.RightNode;
            }

            return node;
        }

        private BinaryTreeNode<T> MinNode(BinaryTreeNode<T> node)
        {
            while (node.LeftNode != null)
            {
                node = node.LeftNode;
            }

            return node;
        }

        private BinaryTreeNode<T> TreeSuccessor(BinaryTreeNode<T> node)
        {
            if (node.RightNode != null)
            {
                return MinNode(node.RightNode);
            }

            var parent = node.ParentNode;
            while (parent != null && node == parent.RightNode)
            {
                node = parent;
                parent = parent.ParentNode;
            }

            return parent;
        }

        private BinaryTreeNode<T> AddNode(BinaryTreeNode<T> node)
        {
            if (root == null)
            {
                root = node;
                return root;
            }

            BinaryTreeNode<T> addAfterNode;

            {
                var currentNode = root;
                do
                {
                    addAfterNode = currentNode;
                    currentNode = compare(node.Data, currentNode.Data) < 0 ? currentNode.LeftNode : currentNode.RightNode;
                } while (currentNode != null);
            }

            node.ParentNode = addAfterNode;

            if (compare(node.Data, addAfterNode.Data) < 0)
            {
                addAfterNode.LeftNode = node;
            }
            else
            {
                addAfterNode.RightNode = node;
            }

            return node;
        }

        private BinaryTreeNode<T> FindNode(BinaryTreeNode<T> node, T data)
        {
            while (node != null && !node.Data.Equals(data))
            {
                if (compare(data, node.Data) > 0)
                {
                    return FindNode(node.RightNode, data);
                }
                else
                {
                    return FindNode(node.LeftNode, data);
                }
            }

            return node;
        }

        private bool RemoveNode(BinaryTreeNode<T> node)
        {

            if (node.LeftNode == null && node.RightNode == null)
            {
                if (node == root)
                {
                    root = null;
                    return true;
                }

                if (node.ParentNode.LeftNode == node)
                {
                    node.ParentNode.LeftNode = null;
                }
                else
                {
                    node.ParentNode.RightNode = null;
                }

                return true;
            }

            if (node.RightNode == null)
            {
                if (node == root)
                {
                    root = node.LeftNode;
                    return true;
                }

                if (node.ParentNode.LeftNode == node)
                {
                    node.ParentNode.LeftNode = node.LeftNode;
                }
                else
                {
                    node.ParentNode.RightNode = node.LeftNode;
                }

                return true;
            }

            if (node.LeftNode == null)
            {
                if (node == root)
                {
                    root = node.RightNode;
                    return true;
                }

                if (node.ParentNode.LeftNode == node)
                {
                    node.ParentNode.LeftNode = node.RightNode;
                }
                else
                {
                    node.ParentNode.RightNode = node.RightNode;
                }

                return true;
            }

            var successor = TreeSuccessor(node);

            if (successor.RightNode == null)
            {
                if (successor.ParentNode.LeftNode == successor)
                {
                    successor.ParentNode.LeftNode = successor.LeftNode;
                }
                else
                {
                    successor.ParentNode.RightNode = successor.LeftNode;
                }
            }
            else
            {
                if (successor.ParentNode.LeftNode == successor)
                {
                    successor.ParentNode.LeftNode = successor.RightNode;
                }
                else
                {
                    successor.ParentNode.RightNode = successor.RightNode;
                }
            }

            if (node == root)
            {
                successor.LeftNode = root.LeftNode;
                successor.RightNode = root.RightNode;
                successor.ParentNode = null;
                root = successor;
                return true;
            }

            if (node.ParentNode.LeftNode == node)
            {
                node.ParentNode.LeftNode = successor;
            }
            else
            {
                node.ParentNode.RightNode = successor;
            }

            successor.LeftNode = node.LeftNode;
            successor.RightNode = node.RightNode;
            successor.ParentNode = node.ParentNode;

            return true;
        }

        private IEnumerable<BinaryTreeNode<T>> PreOrderTraversal(BinaryTreeNode<T> node)
        {
            yield return node;
            if (node.LeftNode != null)
            {
                foreach (var treeNode in PreOrderTraversal(node.LeftNode))
                {
                    yield return treeNode;
                }
            }

            if (node.RightNode != null)
            {
                foreach (var treeNode in PreOrderTraversal(node.RightNode))
                {
                    yield return treeNode;
                }
            }
        }

        private IEnumerable<BinaryTreeNode<T>> InOrderTraversal(BinaryTreeNode<T> node)
        {
            if (node.LeftNode != null)
            {
                foreach (var treeNode in InOrderTraversal(node.LeftNode))
                {
                    yield return treeNode;
                }
            }

            yield return node;

            if (node.RightNode != null)
            {
                foreach (var treeNode in InOrderTraversal(node.RightNode))
                {
                    yield return treeNode;
                }
            }
        }

        private IEnumerable<BinaryTreeNode<T>> PostOrderTraversal(BinaryTreeNode<T> node)
        {
            if (node.LeftNode != null)
            {
                foreach (var treeNode in PostOrderTraversal(node.LeftNode))
                {
                    yield return treeNode;
                }
            }

            if (node.RightNode != null)
            {
                foreach (var treeNode in PostOrderTraversal(node.RightNode))
                {
                    yield return treeNode;
                }
            }

            yield return node;
        }

    }
    public enum TraverseType
    {
        InOrder,
        PreOrder,
        PostOrder
    }

    internal class BinaryTreeNode<T>
    {
        public T Data { get; set; }

        public BinaryTreeNode<T> ParentNode { get; set; }

        public BinaryTreeNode<T> RightNode { get; set; }

        public BinaryTreeNode<T> LeftNode { get; set; }

        public BinaryTreeNode(T data)
        {
            ParentNode = null;
            RightNode = null;
            LeftNode = null;
            Data = data;
        }
    }
}
